# DETAILS

This project is a basic setup to run a monolythic rails application with Docker.
It consists on running 2 containers, one for the Postgresql Server with the persistent data volumes and another for the rails application with a pointing at the rails-app folder, for development purposes.

# run

1. $ docker-compose build
2. $ docker-compose up
